#!/usr/bin/env python2.5
# -*- coding: utf-8 -*-
#
#       This file is part of timelinePkgVersion
#
#       timelinePkgVersion is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from optparse import OptionParser
import sys, urllib, gzip
from time import sleep

tmpPackagesIndex = '/tmp/packages.gz'

optParser = OptionParser()
optParser.add_option("-u", "--repourl", dest="repourl", help ="URL for the reposity")
optParser.add_option("-p", "--package", dest="package", help ="Name of the package")
(opts, args) = optParser.parse_args()
if (not opts.repourl or not opts.package) :
    sys.stderr.write('-u, -p are needed')
    sys.exit(2)

packageIndexUrl = opts.repourl+'/Packages.gz'
try :
    #Download en open package index
    urllib.urlretrieve(packageIndexUrl,tmpPackagesIndex)
    dbFp = gzip.open(tmpPackagesIndex)
    # urllib.urlopen(packageDBurl)
except Exception, e :
    sys.stderr.write(e.message)
    sys.stderr.write('Error on package index download (%s). Check repo url.'%packageIndexUrl)
    sys.exit(3)

inPkgDesc = False
for line in dbFp.readlines() :
    if inPkgDesc and line[0:8] == 'Version:' :
        sys.stdout.write(line[9:].strip())
        sys.exit(0)
    elif line[0:8] == 'Package:' and line[9:].strip() == opts.package:
        inPkgDesc = True

sys.stdout.write('no found')
sys.exit(0)

