#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This file is part of timelinePkgVersion
#
#       timelinePkgVersion is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from optparse import OptionParser
import sys, urllib
from modules import iso8601
from xml.dom import minidom

tmpPackagesIndex = '/tmp/packages.gz'

optParser = OptionParser()
optParser.add_option("-u", "--repourl", dest="repourl", help ="URL for a github atom feed")
(opts, args) = optParser.parse_args()
if (not opts.repourl) :
    sys.stderr.write('option -u required')
    sys.exit(2)

#download atom feed
try :
    atomItems = minidom.parse(urllib.urlopen(opts.repourl)).getElementsByTagName('entry')
except Exception, e :
    sys.stderr.write(e.message)
    sys.exit(3)

#Parse atom feed
items = []
for atomItem in atomItems :
    items.append({
            'id': atomItem.getElementsByTagName('id')[0].firstChild.data,
            'date': iso8601.parse_date(atomItem.getElementsByTagName('updated')[0].firstChild.data)
        })

#Sort item by date
def sorteByDate(a,b) :
    if a['date'] < b['date'] :
        return +1
    else :
        return -1
items.sort(sorteByDate)

sys.stdout.write(items[0]['id'].split('::')[-1])
sys.exit(0)

