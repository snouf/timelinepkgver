#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This file is part of timelinePkgVersion
#
#       timelinePkgVersion is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from optparse import OptionParser
import sys, urllib, tarfile, os
from datetime import datetime

tmpPacman = '/tmp/pacman.db.tgz'

optParser = OptionParser()
optParser.add_option("-u", "--repourl", dest="repourl", help ="URL for the reposity. Example: http://mir.archlinux.fr/core/os/x86_64/ ")
optParser.add_option("-r", "--release", dest="release", help ="Packages database for release of distribution. Example: core")
optParser.add_option("-p", "--package", dest="package", help ="Name of the package. Example: wget.")
optParser.add_option("-t", "--type_tb", dest="dbtype", help ="Type of package database (.db, .db.tar.gz) .db.tar.gz bay default")
(opts, args) = optParser.parse_args()
if (not opts.repourl or not opts.package or not opts.release ) :
    sys.stderr.write('options -u, -p, -r required')
    sys.exit(2)
if (not opts.dbtype) :
    opts.dbtype = '.db.tar.gz'

#download and untar package index
urlPacman = opts.repourl+opts.release+opts.dbtype
try :
    urllib.urlretrieve(urlPacman,tmpPacman)
except Exception, e :
    sys.stderr.write(e.message)
    sys.exit(3)

#finf pgk desc
tar = tarfile.open(tmpPacman)
for pkg in tar.getmembers() :
    if os.path.split(pkg.name)[-1] == 'desc' :
        for keyvalue in tar.extractfile(pkg).read().split("\n\n%") :
            (key,value) = keyvalue.split("%\n",1)
            if key == 'NAME' :
                pgkname = value
            elif key == 'VERSION' :
                pkgversion = value
            elif key == 'BUILDDATE' :
                pkgbuilddate = value
        if pgkname == opts.package :
            sys.stdout.write('%s (%s)'%(pkgversion,datetime.fromtimestamp(int(pkgbuilddate)).strftime('%d/%m/%Y')))
            sys.exit(0)

sys.stderr.write('package not found')
sys.exit(1)
