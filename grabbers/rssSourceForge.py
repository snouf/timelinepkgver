#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This file is part of timelinePkgVer
#
#       timelinePkgVersion is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from optparse import OptionParser
import sys, urllib, re, rfc822
from time import mktime
from xml.dom import minidom

optParser = OptionParser()
optParser.add_option("-u", "--repourl", dest="repourl", help ="URL for a sourcefourge rss files feed. By Example: http://sourceforge.net/api/file/index/project-id/370190/mtime/desc/limit/20/rss")
optParser.add_option("-s", "--search", dest="search", help ="Rationnal expression for find version in title of item feed. By Example: .*MythBackend-(.*).dmg")
(opts, args) = optParser.parse_args()
if (not opts.repourl or not opts.search) :
    sys.stderr.write('options -u and -s required. Use -h for more information')
    sys.exit(2)

re_search = re.compile(opts.search)

#download rss feed
try :
    rssItems = minidom.parse(urllib.urlopen(opts.repourl)).getElementsByTagName('item')
except Exception, e :
    sys.stderr.write(e.message)
    sys.exit(3)

#Parse atom feed
items = []
for rssItem in rssItems :
    title = rssItem.getElementsByTagName('title')[0].firstChild.data
    match = re_search.search(title)
    if match :
        items.append({
                'version': match.groups()[0],
                'date': mktime(rfc822.parsedate(rssItem.getElementsByTagName('pubDate')[0].firstChild.data))
            })

#Sort item by date
def sorteByDate(a,b) :
    if a['date'] < b['date'] :
        return +1
    else :
        return -1
items.sort(sorteByDate)

if (len(items) > 0):
    sys.stdout.write(items[0]['version'])
    sys.exit(0)
else :
    sys.stderr.write('package no found')
    sys.exit(1)
