#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This file is part of timelinePkgVersion
#
#       timelinePkgVersion is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from optparse import OptionParser
import sys, urllib, gzip, re
from datetime import datetime
from xml.dom import minidom

tmpPackagesGzPath = '/tmp/suse.xml.gz'

optParser = OptionParser()
optParser.add_option("-u", "--repourl", dest="repourl", help ="URL for the reposity. Example http://packman.inode.at/suse/openSUSE_12.1/Essentials/x86_64/")
optParser.add_option("-p", "--package", dest="package", help ="Name of the package (rationnal expression supported). Example mplayer")
(opts, args) = optParser.parse_args()
if (not opts.repourl or not opts.package) :
    sys.stderr.write('options -u, -p, -r required')
    sys.exit(2)

repomdXmlUrl = opts.repourl+'../repodata/other.xml.gz'
arch = opts.repourl.strip("/").split("/")[-1]

try :
    urllib.urlretrieve(repomdXmlUrl,tmpPackagesGzPath)
except Exception, e :
    sys.stderr.write(e.message)
    sys.exit(3)

dbFp = gzip.open(tmpPackagesGzPath)
docxml = minidom.parse(dbFp)
rePattern = re.compile(r'%s'%opts.package)
for pkgNode in docxml.firstChild.childNodes :
    if pkgNode.nodeType == pkgNode.ELEMENT_NODE :
        pkgname = pkgNode.getAttribute('name')
        if (rePattern.match(pkgname)) and (pkgNode.getAttribute('arch') == arch) :
            versionNode = pkgNode.getElementsByTagName('version')[0]
            changelogNode = pkgNode.getElementsByTagName('changelog')[0]
            sys.stdout.write('%s (%s)'%(versionNode.getAttribute('ver'),datetime.fromtimestamp(int(changelogNode.getAttribute('date'))).strftime('%d/%m/%Y')))
            sys.exit(0)

sys.stderr.write('package not found')
sys.exit(1)
