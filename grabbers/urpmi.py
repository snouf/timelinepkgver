#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This file is part of timelinePkgVer
#
#       timelinePkgVersion is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from optparse import OptionParser
import sys, urllib, re
from modules import synthesis

tmpSynthesis = '/tmp/synthesis.cz'

optParser = OptionParser()
optParser.add_option("-p", "--package", dest="package", help ="Name of the package. Rationnal expression supported")
optParser.add_option("-s", "--synthesisurl", dest="synthesisurl", help ="Synthesis db url.")
(opts, args) = optParser.parse_args()
if (not opts.synthesisurl or not opts.package) :
    sys.stderr.write('options -s and -p requierd. Use option -h for more information.')
    sys.exit(2)

package = re.compile(opts.package)

try :
    urllib.urlretrieve(opts.synthesisurl,tmpSynthesis)
except Exception, e :
    sys.stderr.write(e.message)
    sys.exit(3)

for synpkg in synthesis.parse(tmpSynthesis) :
    match = package.search(synpkg['name'])
    if match :
        if match.groups()[0] :
            sys.stdout.write(match.groups()[0]+'-'+synpkg['version']+'-'+synpkg['release'])
        else :
            sys.stdout.write(synpkg['version']+'-'+synpkg['release'])
        sys.exit(0)

sys.stderr.write('package not found')
sys.exit(1)
