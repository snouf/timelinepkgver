#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This file is part of timelinePkgVersion
#
#       timelinePkgVersion is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

from optparse import OptionParser
import sys, urllib, sqlite3, bz2, gzip
from xml.dom import minidom
from datetime import datetime
from StringIO import StringIO


tmpfileDBbz2 = '/tmp/yum.sqlite.z'
tmpfileDB = '/tmp/yum.sqlite'

optParser = OptionParser()
optParser.add_option("-u", "--repourl", dest="repourl", help ="URL for the reposity")
optParser.add_option("-p", "--package", dest="package", help ="Name of banche (unused)")
(opts, args) = optParser.parse_args()
if (not opts.repourl or not opts.package) :
    sys.stderr.write('options -u, -p required')
    sys.exit(2)

#Find url packages database
repomdXmlUrl = opts.repourl+'repodata/repomd.xml'
try :
    for dataNode in minidom.parse(urllib.urlopen(repomdXmlUrl)).getElementsByTagName('data') :
        if dataNode.getAttribute('type') == 'primary' :
            urlDB = opts.repourl+'/'+dataNode.getElementsByTagName('location')[0].getAttribute('href')
            break
        elif dataNode.getAttribute('type') == 'primary_db' :
            urlDB = opts.repourl+'/'+dataNode.getElementsByTagName('location')[0].getAttribute('href')
            break
except Exception, e :
    sys.stderr.write(e.message)
    sys.exit(4)

if urlDB[-7:] == '.xml.gz' :
    inmemory = StringIO(urllib.urlopen(urlDB).read())
    for dataNode in minidom.parse(gzip.GzipFile(fileobj=inmemory, mode='rb')).getElementsByTagName('package') :
        if dataNode.getElementsByTagName('name')[0].firstChild.data == opts.package :
            verNode = dataNode.getElementsByTagName('version')[0]
            sys.stdout.write('%s-%s'%(verNode.getAttribute('ver'), verNode.getAttribute('rel')))
            sys.exit(0)
            break
    sys.stderr.write('package not found')
    sys.exit(1)


else : #sqlite.bz2
    #download end open packages database
    try :
        urllib.urlretrieve(urlDB,tmpfileDBbz2)
    except Exception, e :
        sys.stderr.write(e.message)
        sys.exit(3)
    fbz2 = bz2.BZ2File(tmpfileDBbz2)
    fp = open(tmpfileDB,'wb')
    fp.write(fbz2.read())
    fp.close()

    #find package
    conn = sqlite3.connect(tmpfileDB)
    cur = conn.cursor()

    cur.execute('SELECT version,time_build FROM packages WHERE name="%s"'%opts.package)
    rep = cur.fetchone()
    if rep :
        sys.stdout.write('%s (%s)'%(rep[0],datetime.fromtimestamp(rep[1]).strftime('%d/%m/%Y')))
        sys.exit(0)
    else :
        sys.stderr.write('package not found')
        sys.exit(1)
