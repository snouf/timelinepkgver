var rootCanvas;
var canvasTimeline;
var canvasChronoLine;
var ctxTimeline;
var ctxChronoLine;
var lenghtTimeline;
var dateStart;
var dateEnd;
var canvasHeight = 20;
var spaceBetweenTimelines = 80;
var marginRight = spaceBetweenTimelines-40;
var marginLeft = 120;
var prevBlockDistrib = false;
var prevTitleDistrib = false;
var labels4chronoLine = new Array;


$(document).ready(function(){
    canvasTimeline = document.getElementById("timeline");
    canvasChronoLine = document.getElementById("showdate");
    canvasTimeline.height = canvasHeight;
    canvasChronoLine.addEventListener('mousemove', mouseHandle, false);
    canvasChronoLine.height = canvasHeight;
    ctxTimeline = canvasTimeline.getContext("2d");
    ctxChronoLine = canvasChronoLine.getContext("2d");
    ctxTimeline.save();
    ctxTimeline.font = "10pt Sans";
    ctxTimeline.strokeStyle = "blue";
    ctxTimeline.fillStyle = "blue";
    ctxChronoLine.font = "10pt Sans";
    ctxChronoLine.textAlign = "right";
    lenghtTimeline = canvasTimeline.width - marginRight - marginLeft ;

    change_selected(43);
})


function change_selected(id) {
    // Change visible list checkbox
    blockDistrib = document.getElementById('block_distrib_'+id);
    titleDistrib = document.getElementById('title_distrib_'+id);
    prevBlockDistrib.className = "block_distrib";
    prevTitleDistrib.className = "title_distrib";
    blockDistrib.className = "block_distrib_selected";
    titleDistrib.className = "title_distrib_selected";
    prevBlockDistrib = blockDistrib;
    prevTitleDistrib = titleDistrib;
}

function checkall() {
    //uncheck all release and all distrib
    for(i=0;i<document.form_display.length;i++) {
        if(document.form_display.elements[i].type=="checkbox")
            document.form_display.elements[i].checked=true;
    }
}

function uncheckall() {
    //uncheck all release and all distrib
    for(i=0;i<document.form_display.length;i++) {
        if(document.form_display.elements[i].type=="checkbox")
            document.form_display.elements[i].checked=false;
    }
}

function updateCanvasHeight() {
    //change the canvas height
    canvasHeight = canvasHeight + spaceBetweenTimelines;
}

function drawTimeline(label,points,pointLeft) {
    ctxTimeline.translate(0,spaceBetweenTimelines);
    ctxTimeline.save();
    tY = -30;

    //draw label
    for(var i = 0; i < label.length; i++){
        ctxTimeline.fillText(label[i],10,tY)
        tY = tY +12; //space betwen line
    }
    ctxTimeline.translate(marginLeft,0);

    //draw line
    ctxTimeline.strokeStyle = "skyblue";
    if (pointLeft) {
        //timeline start on left
        ctxTimeline.moveTo(0,0);
        ctxTimeline.lineTo(lenghtTimeline, 0);
    } else if (pts.length > 0) {
        //timeline start here
        posX = (pts[0][1]-dateStart)/(dateEnd-dateStart)*lenghtTimeline;
        ctxTimeline.moveTo(posX,0);
        ctxTimeline.lineTo(lenghtTimeline, 0);
    } else {
        // no data no timeline
        ctxTimeline.moveTo(0,0);
        ctxTimeline.fillText('no data', 6, 4);
    }
    ctxTimeline.stroke();

    // an array for chronoline labels
    var labels4chronoLineX = new Array;

    //drawpoint
    for (i in pts) {
        label=pts[i][0];
        date=pts[i][1];
        posX = (date-dateStart)/(dateEnd-dateStart)*lenghtTimeline;
        ctxTimeline.save();
        ctxTimeline.translate(posX, 0);
        ctxTimeline.moveTo(0,0);
        ctxTimeline.arc(0,0,4,2*Math.PI,0);
        ctxTimeline.fill();
        ctxTimeline.rotate(-Math.PI/4);
        ctxTimeline.fillStyle = "blue";
        ctxTimeline.font = "8pt Sans";
        if (label.length < 12)
            ctxTimeline.fillText(label, 6, 4);
        else
            ctxTimeline.fillText(label.substr(0,10)+'...', 6, 4);
        ctxTimeline.stroke();
        ctxTimeline.fill();
        ctxTimeline.restore();
        labels4chronoLineX.push([label,posX+marginLeft]);
    }
    // add value un array for chronoLine labels
    labels4chronoLine.push([(labels4chronoLine.length+1)*spaceBetweenTimelines,labels4chronoLineX.reverse(),pointLeft]);

    ctxTimeline.restore();
}

function drawChronoLine (x,y) {
    //draw vertical line
    x = Math.max(marginLeft,Math.min(marginLeft+lenghtTimeline,x));
    date = new Date((dateStart+(dateEnd-dateStart)*(x-marginLeft)/lenghtTimeline)*1000);
    ctxChronoLine.clearRect(0,0,canvasChronoLine.width,canvasChronoLine.height);
    ctxChronoLine.fillStyle = "red";
    ctxChronoLine.fillRect(x-1,0,2,canvasChronoLine.height);

    //write date
    ctxChronoLine.save();
    ctxChronoLine.translate(x,y);
    ctxChronoLine.rotate(-Math.PI/2);
    ctxChronoLine.textAlign = "left";
    ctxChronoLine.fillText('Date: '+date.getDate().toString()+'/'+(date.getMonth()+1).toString()+'/'+date.getFullYear().toString(),10,12);
    ctxChronoLine.restore();

    //write pagages versions
    for (l in labels4chronoLine) {
        label = 'no data';
        if (labels4chronoLine[l][2])
            label = labels4chronoLine[l][2] //display value on left
        for (c in labels4chronoLine[l][1]) {
            if (x > labels4chronoLine[l][1][c][1]) {
                label = labels4chronoLine[l][1][c][0]; //display package version before this date
                break;
            }
        }
        //write
        ctxChronoLine.save();
        ctxChronoLine.translate(x,labels4chronoLine[l][0]);
        ctxChronoLine.moveTo(0,0);
        ctxChronoLine.textAlign = "right";
        ctxChronoLine.font = "8pt Sans";
        ctxChronoLine.fillStyle = "black";
        ctxChronoLine.fillText(label, -6, 15);
        ctxChronoLine.restore();
    }
}

function mouseHandle(ev) {
    //mouse coords
    var mouseX = 0;
    var mouseY = 0;
    if (ev.layerX || ev.layerX == 0) { // Firefox
        mouseX = ev.layerX;
        mouseY = ev.layerY;
    } else if (ev.offsetX || ev.offsetX == 0) { // Opera
        mouseX = ev.offsetX;
        mouseY = ev.offsetY;
    }
    drawChronoLine(mouseX,mouseY)
}


function exportPNG() {
    //fusion canvas en write png
    ctxTimeline.restore();
    ctxTimeline.drawImage(canvasChronoLine,0,0);
    window.location = canvasTimeline.toDataURL("image/png");
}
