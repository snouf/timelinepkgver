<?php
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


//config
$dateformat = '%d/%m/%Y';
$sqlite_file = 'timelinePkgVer.sqlite';

//functions
function checkbox($name,$label, $checked=False) {
    if ($checked)
        return '<input type="checkbox" id="'.$name.'" name="'.$name.'" checked="checked" /><label for="'.$name.'">'.$label.'</label>';
    else
        return '<input type="checkbox" id="'.$name.'" name="'.$name.'" /><label for="'.$name.'">'.$label.'</label>';
}

function input_text($name, $value) {
    return '<input type="text" name="'.$name.'" value="'.$value.'" />';
}

//arguments
$visible = array();
if (isset($_GET['visible'])) {
    foreach (explode(',',$_GET['visible']) as $id)
        $visible[] = intval($id);
}
$to = 0;
if (isset($_GET['to'])) {
    $datetime = strptime($_GET['to'],$dateformat);
    $to = mktime(23 /*h*/, 59 /*min*/, 59 /*sec*/, $datetime['tm_mon'] + 1, $datetime['tm_mday'], $datetime['tm_year'] + 1900);
}
if (!$to)
    $to = mktime();
$from = 0;
if (isset($_GET['from'])) {
    $datetime = strptime($_GET['from'],$dateformat);
    $from = mktime(23 /*h*/, 59 /*min*/, 59 /*sec*/, $datetime['tm_mon'] + 1, $datetime['tm_mday'], $datetime['tm_year'] + 1900);
}
if (!$from)
    $from = $to-30*24*60*60;

//open database
$db = new SQLite3($sqlite_file);

if (isset($_POST['submit'])) {
    //form data
    $results = $db->query('SELECT id FROM repos');
    $visible = array();
    while ($repo = $results->fetchArray(SQLITE3_ASSOC)) {
        if ((isset($_POST['show_repo_'.$repo['id']])) && ($_POST['show_repo_'.$repo['id']]))
            $visible[] = $repo['id'];
    }

    if (isset($_POST['to']))
        $to = addslashes($_POST['to']);
    else
        $to = '';

    if (isset($_POST['from']))
        $from = addslashes($_POST['from']);
    else
        $from = '';

    //redirection
    header('Location:http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'?from='.$from.'&to='.$to.'&visible='.join(',',$visible));
} elseif (isset($_POST['default'])) {
    header('Location:http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
}

//Display
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>tilelinePkgVer</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/timelinePkgVer.js"></script>
    <link rel="alternate stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
    <fieldset>
    <form name="form_display" method="post" action="http://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'] ?>">
        <!-- Formulaire date -->
        <fieldset>
            <legend>Période</legend>
            <p>
                Du <?php echo input_text('from',strftime($dateformat,$from)) ?>
                au <?php echo input_text('to',strftime($dateformat,$to)) ?>
                <i>Format des dates : JJ/MM/AAAA</i>
            </p>
        </fieldset>

        <!-- Formulaire distribution -->
        <fieldset>
            <legend>Distributions</legend>
                <div class="distribs">
<?php
    $distrib = '';
    $arr_repo = array();
    $results = $db->query('SELECT id,distrib,release,repo_name,repo_url,visible FROM repos ORDER BY distrib,release,sort,repo_name');
    while ($repo = $results->fetchArray(SQLITE3_ASSOC)) {
        $is_visible = (isset($_GET['visible']) ? in_array($repo['id'],$visible) : $repo['visible']) ;
        if ($is_visible)
            $arr_repo[$repo['id']] = "['".addslashes($repo['distrib'].' '.$repo['release'])."','".addslashes($repo['repo_name'])."']";

        if ($repo['distrib'] != $distrib) {
            if ($distrib != '')
                echo '
                            </ul>
                        </div>
                    </div>';
        echo '
                    <h3 class="title_distrib" id="title_distrib_'.$repo['id'].'" onclick="change_selected('.$repo['id'].')">'.$repo['distrib'].'</h3>
                    <div class="block_distrib" id="block_distrib_'.$repo['id'].'">
                ';
        $release = '';
    }

    if ($repo['release'] != $release) {
        if ($release != '') echo '
                            </ul>
                        </div>';
        echo '
                        <div class="block_release">
                            <h4>'.$repo['release'].'</h4>
                            <ul>';
    }

    echo '
                                <li>'.checkbox('show_repo_'.$repo['id'],$repo['repo_name'],$is_visible).' <a href="'.$repo['repo_url'].'">URL</a></li>';

    $release = $repo['release'];
    $distrib = $repo['distrib'];
}
?>
                            </ul>
                        </div>

                    </div>
                    <a href="#" onclick="checkall(); return false;">Tout cocher</a>
                    <a href="#" onclick="uncheckall(); return false;">Tout décocher</a>
                </div>
            </fieldset>
            <input type="submit" name="submit" value="afficher" />
            <input type="submit" name="defaut" value="Valeurs par défaut" />
        </form>
    </fieldset>

    <!-- canvas -->
    <div id="canvas">
        <canvas id="timeline" width="1000" height="1000"></canvas>
        <canvas id="showdate" width="1000" height="1000"></canvas>
        <canvas id="pointhover" width="4" height="4"></canvas>
    </div>
    <a href="#" onclick="exportPNG();return false;">Export PNG</a>
    <script>

<?php
echo '
        dateStart = '.$from.';
        dateEnd = '.$to.';';

foreach ($arr_repo as $repo_id => $repo_label) {
    $pts = array();

    // points of timeline
    $results = $db->query('SELECT * FROM timeline WHERE repo_id='.$repo_id.' AND date>'.$from.' AND date<'.$to.' ORDER BY date');
    while ($pt_time = $results->fetchArray(SQLITE3_ASSOC)) {
        if(!isset($pt_time['id'])) continue;
        $pts[] = '["'.$pt_time['pkg_ver'].'",'.$pt_time['date'].']';
    }

    //first value on left time line
    $results = $db->query('SELECT * FROM timeline WHERE repo_id='.$repo_id.' AND date<'.$from.' AND date<'.$to.' ORDER BY date DESC LIMIT 1');
    $pt_time = $results->fetchArray(SQLITE3_ASSOC);
    if(isset($pt_time['id']))
        $ptLeft = '["'.$pt_time['pkg_ver'].'",'.$pt_time['date'].']';
    else
        $ptLeft = 'false';

    echo '
        // timemine for '.$repo_label.' ('.$repo_id.')
        updateCanvasHeight();
        $(document).ready(function(){
            ptLeft = '.$ptLeft.';
            pts = new Array('.join(',',$pts).');
            drawTimeline('.$repo_label.',pts,ptLeft);
        });';
}
?>

        $(document).ready(function(){
            drawChronoLine(99999,120);
        });

     </script>
     <div class="footer">
        Powered by <a href="http://jonas.tuxfamily.org/wiki/timelinepkgver">TimelinePkgVer</a>
     </div>
</body></html>
