#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       This file is part of timelinePkgVer
#
#       timelinePkgVersion is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.


__title__ ="timelinePkgVersion";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="dev"

#config
sqlite_db = "timelinePkgVer.sqlite"

import sqlite3, os
from subprocess import Popen,PIPE
from time import time
import logging

os.chdir(os.path.dirname(__file__))
logging.basicConfig(level=logging.INFO,format='%(asctime)s %(levelname)s - %(message)s',pathname='timelinePkgVersion.log')
logger = logging.getLogger(__title__)

conn = sqlite3.connect(sqlite_db)
c1 = conn.cursor()
c2 = conn.cursor()

#~ c1.execute('select id,distrib,release,repo_name,repo_url,pkg_name,grabber from repos where grabbable=1')
c1.execute('select id,distrib,release,repo_name,repo_url,pkg_name,grabber from repos where distrib = "Windows"')
for (repo_id,distrib,release,repo_name,repo_url,pkg_name,grabber) in c1 :
    logger.info('Update %s (%s, %s)'%(repo_name,distrib,release))
    cmd = grabber%{'repo_url':repo_url,'pkg_name':pkg_name,'release':release,'repo_name':repo_name}
    logger.debug("exec: "+cmd)
    pipe = Popen(cmd,shell=True,stdout=PIPE, stderr=PIPE, close_fds=True
        )
    sterr = pipe.stderr.read()

    if pipe.wait() == 0 :
        if sterr :
            logger.warning(sterr)
        this_pgk_ver = pipe.stdout.read()
        if this_pgk_ver :
            c2.execute('select pkg_ver from timeline where repo_id=%i order by date desc limit 1'%repo_id)
            prev_pgk_ver = c2.fetchone()
            if prev_pgk_ver :
                prev_pgk_ver = prev_pgk_ver[0]
            logger.debug('actual pgkver:     "%s"'%this_pgk_ver)
            logger.debug('previous pgkver:   "%s"'%prev_pgk_ver)
            if (not prev_pgk_ver) or (this_pgk_ver != prev_pgk_ver) :
                logger.info('Append new package version')
                c2.execute('insert into timeline (repo_id, date, pkg_ver) values (%i,%i,"%s")'%(repo_id,int(time()),this_pgk_ver))
            else :
                logger.info('Package not update')
    else :
        logger.error(sterr)

conn.commit()

c1.close()
c2.close()
